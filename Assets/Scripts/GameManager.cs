using UnityEngine;
using Zenject;

public class GameManager : MonoBehaviour
{
    public CardsList MasterCardsList;

    private PlayerStack _playerStack;
    private PlayerStack _opponentStack;
    private GameFSM _fsm;

    [Inject]
    private void Initialise([Inject(Id = "Player")] PlayerStack playerStack,
        [Inject(Id = "Opponent")] PlayerStack opponentStack,
        GameFSM fsm)
    {
        _playerStack = playerStack;
        _opponentStack = opponentStack;

        Debug.Log($"PlayerStack = {playerStack.name}");
        Debug.Log($"OpponentStack = {opponentStack.name}");
        _fsm = fsm;
    }


    // Start is called before the first frame update
    private void Start()
    {
        //  we should be in GameStart state here
        if (_fsm.CurrentState == GameFSM.GameStates.GameStart)
        {
            _fsm.IssueCommand("EndPhase");
        }

        //if  (MasterCardsList != null)
        //{

        //    foreach (CardDefinition card in MasterCardsList.Cards)
        //    {
        //        Debug.Log($"Found Card:{card.name}");
        //    }
        //}
    }

}