using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    public PlayerStack PlayerStack;
    public PlayerStack OpponentStack;

    public override void InstallBindings()
    {
        Container.Bind<GameFSM>().FromNew().AsSingle();

        Container.Bind<PlayerStack>().WithId("Player").FromInstance(PlayerStack);
        Container.Bind<PlayerStack>().WithId("Opponent").FromInstance(OpponentStack);
    }
}