using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class MainCanvas : MonoBehaviour
{
    public Button EndTurnButton;
    private GameFSM FSM;
    private bool _turnEnded;

    [Inject]
    private void Derp(GameFSM fsm)
    {
        FSM = fsm;
    }

    private void Awake()
    {
        EndTurnButton.onClick.AddListener(EndTurnButtonClick);
        FSM.OnEnterPhase += FSM_OnEnterPhase;
    }

    private void FSM_OnEnterPhase(GameFSM.GameStates state)
    {
        bool shouldShowButton = (state == GameFSM.GameStates.PlayerPlayPhase);
        EndTurnButton.gameObject.SetActive(shouldShowButton);
    }

    public void EndTurnButtonClick()
    {
        FSM.IssueCommand("EndTurn");
    }
}
