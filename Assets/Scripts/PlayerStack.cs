using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlayerStack : MonoBehaviour
{
    public CardPlacement[] CardPositions;
    public CardInstance CardInstancePrefab;
    public CardsList MasterCardsList;
    public bool IsEnemy;
    public Transform HandPosition;
    
    [Inject] private GameFSM FSM;

    private List<CardInstance> _handCards = new List<CardInstance>();
    private List<CardInstance> _tableCards = new List<CardInstance>();
    private CardInstance _selectedCard;
    private const float MaxPosition = 10f;
    private GameFSM.GameStates _currentState;
    private CardPlacement _selectedPlacement;

    private void Awake()
    {
        FSM.OnEnterPhase += FSM_OnEnterPhase;

        foreach (CardPlacement placement in CardPositions)
        {
            placement.OnClicked += HandleOnCardPlacementClicked;
        }   
    }

    private void HandleOnCardPlacementClicked(CardPlacement placement)
    {
        _selectedPlacement = placement;
        Debug.Log($"_selectedPlacement = {_selectedPlacement}");

        CardPlacing();
    }

    private void FSM_OnEnterPhase(GameFSM.GameStates state)
    {
        if (state == GameFSM.GameStates.PlayerPlayPhase)
        {
            _currentState = state;
        }
        if (state == GameFSM.GameStates.PlayerEndPhase)
        {
            _currentState = state;
            //Debug.Log("unhighlighting");
            foreach (CardInstance card in _handCards)
            {
                card.HighlightCard(false);
            }
            foreach (CardInstance card in _tableCards)
            {
                card.HighlightCard(false);
            }
        }
    }

    public void AddRandomCard()
    {
        CardDefinition definition = MasterCardsList.GetRandomCard();
        if (definition != null)
        {
            AddCard(definition);
        }
    }


    public CardInstance AddCard(CardDefinition card)
    {
        CardInstance cardInstance = GameObject.Instantiate<CardInstance>(CardInstancePrefab);
        cardInstance.SetCard(card);
        cardInstance.transform.SetParent(HandPosition, false);
        cardInstance.OnCardClicked += Handle_OnCardClicked;
        _handCards.Add(cardInstance);
        RearrangeCardsInHand();
        return cardInstance;
    }

    private void Handle_OnCardClicked(CardInstance cardClicked)
    {
        if (IsEnemy)
        {
            return;
        }

        //  un-highlight all cards
        foreach (CardInstance card in _handCards)
        {
            card.HighlightCard(false);
        }

        foreach (CardInstance card in _tableCards)
        {
            card.HighlightCard(false);
        }

        if (_currentState == GameFSM.GameStates.PlayerPlayPhase)
        {
            if (_selectedCard == cardClicked)
            {
                _selectedCard = null;
            }
            else
            {
                cardClicked.HighlightCard(true);
                _selectedCard = cardClicked;
                if ((_selectedPlacement != null) && (_selectedCard != null))
                {
                    Debug.Log($"Placing {_selectedCard} on {_selectedPlacement}");
                    
                }
                
            }
        }
    }

    private void CardPlacing()
    {
        if (_selectedCard == null)
        {
            return;
        }
        else
        {
            _handCards.Remove(_selectedCard);
            _tableCards.Add(_selectedCard);
            _selectedCard.transform.SetParent(_selectedPlacement.transform, false);
            _selectedCard.transform.localPosition = Vector3.zero;
            _selectedCard.HighlightCard(false);
            _selectedCard = null;
        }
    }

    private void RearrangeCardsOnTable()
    {
        int cardCount = _handCards.Count;

        if (_handCards.Count < CardPositions.Length)
        {
            for (int i = 0; i < cardCount; i++)
            {
                _handCards[i].transform.SetParent(CardPositions[i].transform, false);
            }
        }

    }



    private void RearrangeCardsInHand()
    {
        int cardCount = _handCards.Count;

        if (cardCount > 0)
        {
            if (cardCount == 1)
            {
                _handCards[0].transform.localPosition = Vector3.zero;
            }
            else
            {

                for (int i = 0; i < cardCount; i++)
                {
                    CardInstance instance = _handCards[i];
                    float stepPercent = (float) i / (cardCount - 1);

                    float posX = Mathf.Lerp(-MaxPosition, MaxPosition, stepPercent);
                    instance.transform.localPosition = new Vector3(posX, 0f, 0f);
                    instance.SetOrder(i);
                }
            }
        }
    }
}
