using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardInstance : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Canvas _frontCanvas;
    [SerializeField] private Canvas _backCanvas;
    [SerializeField] private Image _renderer;
    [SerializeField] private TMPro.TextMeshProUGUI _nameText;
    [SerializeField] private TMPro.TextMeshProUGUI _manaCost;
    [SerializeField] private TMPro.TextMeshProUGUI _strength;
    [SerializeField] private TMPro.TextMeshProUGUI _health;

    [NonSerialized] public CardDefinition Card;

    public event Action<CardInstance> OnCardClicked;

    private void Reset()
    {
        _backCanvas = transform.Find("BackCanvas").GetComponent<Canvas>();
        _frontCanvas = transform.Find("FrontCanvas").GetComponent<Canvas>();
        _renderer = transform.Find("FrontCanvas/CardImage").GetComponent<Image>();
        _nameText = transform.Find("FrontCanvas/CardName").GetComponent<TMPro.TextMeshProUGUI>();
        _manaCost = transform.Find("FrontCanvas/ManaBox/ManaCost").GetComponent<TMPro.TextMeshProUGUI>();
        _strength = transform.Find("FrontCanvas/Strength").GetComponent<TMPro.TextMeshProUGUI>();
        _health = transform.Find("FrontCanvas/Health").GetComponent<TMPro.TextMeshProUGUI>();
    }

    private void Awake()
    {
        _backCanvas.worldCamera = Camera.main;
        _frontCanvas.worldCamera = Camera.main;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (Card != null)
        {
            SetCard(Card);
        }
    }

    public void SetCard(CardDefinition card)
    {
        Card = card;
        _renderer.sprite = Card.CardSprite;
        _nameText.text = Card.name;
        _strength.text = Card.Attack.ToString();
        _manaCost.text = Card.Cost.ToString();
        _health.text = Card.Defense.ToString();
    }

    public void SetOrder(int order)
    {
        _backCanvas.sortingOrder = order;
        _frontCanvas.sortingOrder = order;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OnCardClicked?.Invoke(this);
    }

    public void HighlightCard(bool isHighlit)
    {
        if (isHighlit)
        {
            transform.localScale = Vector3.one * 1.5f;
            SetOrder(1000);
        }
        else
        {
            transform.localScale = Vector3.one;
        }
    }
}
