using UnityEngine;

[CreateAssetMenu(fileName ="MasterCardsList", menuName ="Cards/CardsList")]
public class CardsList : ScriptableObject
{
    public CardDefinition[] Cards;

    public CardDefinition GetRandomCard()
    {
        if (Cards != null && Cards.Length > 0)
        {
            int cardNum = UnityEngine.Random.Range(0, Cards.Length);
            return Cards[cardNum];
        }
        return null;
    }
}
