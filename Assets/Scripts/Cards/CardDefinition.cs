using UnityEngine;

[CreateAssetMenu(fileName ="Card", menuName ="Cards/New Card")]
public class CardDefinition : ScriptableObject
{
    public int Cost;
    public int Attack;
    public int Defense;
    public Sprite CardSprite;
}
