﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardPlacement : MonoBehaviour, IPointerClickHandler
{ 
    public event Action<CardPlacement> OnClicked;

    public void OnPointerClick(PointerEventData eventData)
    {
        //Debug.Log($"Clicked on {name}");
        OnClicked?.Invoke(this);
    }
}
