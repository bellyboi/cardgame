﻿using System.Collections;
using UnityEngine;
using Zenject;

public class EnemyController : UserController
{
    [Inject]
    protected void Initialise(
        [Inject(Id = "Opponent")] PlayerStack opponentStack,
        GameFSM fsm)
    {
        IsEnemy = true;
        _stack = opponentStack;
        _fsm = fsm;
    }

    protected override void RunPlayPhase()
    {
        IssueCommand("EndPhase");
    }
}