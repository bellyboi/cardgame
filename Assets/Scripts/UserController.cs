﻿using System;
using System.Collections;
using UnityEngine;
using Zenject;

public abstract class UserController : MonoBehaviour
{
    protected GameFSM _fsm;
    protected PlayerStack _stack;
    public int CardsPerDraw = 1;
    [NonSerialized] public bool IsEnemy;




    protected virtual void Awake()
    {
        _fsm.OnEnterPhase += FSM_OnEnterPhase;
    }

    private void FSM_OnEnterPhase(GameFSM.GameStates newState)
    {
        //string playerName = IsEnemy ? "Enemy" : "Player";

        //Debug.Log($"{playerName} state = {newState}");
        if (IsEnemy)
        {
            switch (newState)
            {
                case GameFSM.GameStates.EnemyStart:
                    RunSetupTasks();
                    break;
                case GameFSM.GameStates.EnemyDrawPhase:
                    RunDrawCards();
                    break;
                case GameFSM.GameStates.EnemyPlayPhase:
                    RunPlayPhase();
                    break;
                case GameFSM.GameStates.EnemyEndPhase:
                    IssueCommand("EndPhase");
                    break;
            }
        }
        else
        {
            switch (newState)
            {
                case GameFSM.GameStates.PlayerStart:
                    RunSetupTasks();
                    break;
                case GameFSM.GameStates.PlayerDrawPhase:
                    RunDrawCards();
                    break;
                case GameFSM.GameStates.PlayerPlayPhase:
                    RunPlayPhase();
                    break;
                case GameFSM.GameStates.PlayerEndPhase:
                    IssueCommand("EndPhase");
                    break;
            }
        }
    }

    protected virtual void RunPlayPhase()
    {
        throw new NotImplementedException();
    }

    protected virtual void RunSetupTasks()
    {
        IssueCommand("EndPhase");
    }

    protected virtual void RunDrawCards()
    {
        for (int i = 0; i < CardsPerDraw; i++)
        {
            _stack.AddRandomCard();
        }
        IssueCommand("EndPhase");
    }

    protected void EndTurn()
    {
        IssueCommand("EndTurn");
    }

    protected void IssueCommand(string command)
    {
        StartCoroutine(IssueCommandDelayed(command));
    }

    private IEnumerator IssueCommandDelayed(string command)
    {
        yield return new WaitForSeconds(0.1f);
        _fsm.IssueCommand(command);
    }
}