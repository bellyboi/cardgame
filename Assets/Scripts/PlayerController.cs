﻿using System;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

public class PlayerController : UserController
{
    [Inject]
    protected void Initialise([Inject(Id = "Player")] PlayerStack playerStack,
            GameFSM fsm)
    {
        _stack = playerStack;
        _fsm = fsm;
    }

    protected override void RunPlayPhase()
    {
    }

}