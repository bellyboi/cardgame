using DUCK.FSM;
using System;
using UnityEngine;

public class GameFSM
{
    public event Action<GameStates> OnEnterPhase;
    public event Action OnPlayerDrawCards;
    public event Action OnEnemyDrawCards;

    private FiniteStateMachine<GameStates> _fsm;

    public GameStates CurrentState => _fsm.CurrentState;

    public enum GameStates
    {
        GameStart,
        PlayerStart,
        PlayerDrawPhase,
        PlayerPlayPhase,
        PlayerEndPhase,
        EnemyStart,
        EnemyDrawPhase,
        EnemyPlayPhase,
        EnemyEndPhase,
        LoseState,
        WinState,
        EndGame
    }

    public GameFSM()
    {
        _fsm = FiniteStateMachine<GameStates>.FromEnum();

        //game begin
        _fsm.AddTransition(GameStates.GameStart, GameStates.PlayerStart, "EndPhase");

        //player states
        _fsm.AddTransition(GameStates.PlayerStart, GameStates.PlayerDrawPhase, "EndPhase");
        _fsm.AddTransition(GameStates.PlayerDrawPhase, GameStates.PlayerPlayPhase, "EndPhase");

        //player to enemy and vise versa
        _fsm.AddTransition(GameStates.PlayerEndPhase, GameStates.EnemyStart, "EndPhase");
        _fsm.AddTransition(GameStates.EnemyEndPhase, GameStates.PlayerStart, "EndPhase");

        //enemy states
        _fsm.AddTransition(GameStates.EnemyStart, GameStates.EnemyDrawPhase, "EndPhase");
        _fsm.AddTransition(GameStates.EnemyDrawPhase, GameStates.EnemyPlayPhase, "EndPhase");
        _fsm.AddTransition(GameStates.EnemyPlayPhase, GameStates.EnemyEndPhase, "EndPhase");

        //special states
        _fsm.AddTransition(GameStates.PlayerPlayPhase, GameStates.PlayerEndPhase, "EndTurn");
        _fsm.AddTransition(GameStates.PlayerPlayPhase, GameStates.LoseState, "PlayerDead");
        _fsm.AddTransition(GameStates.PlayerPlayPhase, GameStates.WinState, "EnemyDead");
        _fsm.AddTransition(GameStates.EnemyPlayPhase, GameStates.WinState, "EnemyDead");
        _fsm.AddTransition(GameStates.EnemyPlayPhase, GameStates.LoseState, "PlayerDead");
        _fsm.AddTransition(GameStates.LoseState, GameStates.EndGame, "EndGame");
        _fsm.AddTransition(GameStates.WinState, GameStates.EndGame, "EndGame");
        _fsm.AddTransition(GameStates.EndGame, GameStates.GameStart, "RestartGame");

        _fsm.OnChange(RaiseOnChangeEvent);
        _fsm.OnEnter(GameStates.PlayerDrawPhase, () => OnPlayerDrawCards?.Invoke());
        _fsm.OnEnter(GameStates.EnemyDrawPhase, () => OnEnemyDrawCards?.Invoke());

        _fsm.Begin(GameStates.GameStart);
    }



    private void RaiseOnChangeEvent(GameStates oldState, GameStates newState)
    {
        OnEnterPhase?.Invoke(newState);
    }

    public void IssueCommand(string command)
    {
        //Debug.Log($"Issued Command {command} during state {_fsm.CurrentState}");
        _fsm.IssueCommand(command);
    }
}
